<?php
   function custom_css_classes_for_vc_row_and_vc_column($class_string, $tag)
   {

        if($tag=='vc_row' || $tag=='vc_row_inner') {
            $class_string = str_replace('vc_row-fluid', 'row', $class_string);
        }

        if($tag=='vc_column' || $tag=='vc_column_inner')
        {
        	$gridColumns	=	24;
        	$old_class_string	=	$class_string;
        	//$columnNum	=	str_replace('vc_col-sm-', '', $class_string);   // comes as:   wpb_column vc_column_container vc_col-sm-4
			$custom_classes	=	preg_replace('/vc_col-sm-(\d{1,2})/', '', $class_string);

            $columnsOn12 	= preg_replace('/vc_col-sm-(\d{1,2})/', '$1', $class_string);
			$colNum			= str_replace($custom_classes,'',$columnsOn12);
            $colNum 		= intval($colNum);
            $newCol			= $colNum*2;

            //$class_string = $custom_classes . " col-xs-$gridColumns col-sm-$newCol col-md-$newCol col-lg-$newCol $offset (" . $old_class_string . ")";
            $class_string = $custom_classes . " col-xs-$gridColumns col-sm-$newCol col-md-$newCol col-lg-$newCol $offset";

        }
        return $class_string;
    }

    // Filter to Replace default css class for vc_row shortcode and vc_column
    add_filter('vc_shortcodes_css_class', 'custom_css_classes_for_vc_row_and_vc_column', 10, 2);

	/*

	function improved_trim_excerpt($text)
	{
        global $post;
        if ( '' == $text )
        {
            $text = get_the_content('');
            $text = apply_filters('the_content', $text);
            $text = str_replace('\]\]\>', ']]&gt;', $text);
            $text = preg_replace('@<script[^>]*?>.*?</script>@si', '', $text);
            $text = strip_tags($text, '<p>');
            $excerpt_length = 80;
            $words = explode(' ', $text, $excerpt_length + 1);
            if (count($words)> $excerpt_length)
            {
                array_pop($words);
                array_push($words, '...');
                $text = implode(' ', $words);
            }
        }
        return $text;
	}
	remove_filter('get_the_excerpt', 'wp_trim_excerpt');
	add_filter('get_the_excerpt', 'improved_trim_excerpt');
*/

    add_action('acf/register_fields', 'my_register_fields');

	function my_register_fields()
	{
	    include_once('acf-nav/nav-menu-v4.php');
	}



	function generateRandomString($length = 10)
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++)
	    {
	        $randomString .= $characters[rand(0, strlen($characters) - 1)];
	    }
	    return $randomString;
	}


	// move admin bar to bottom
	function fb_move_admin_bar() { ?>
		<style type="text/css">
			html
			{
				margin-top:0 !important;
			}
		</style>
	<?php }
	// on backend area
	//add_action( 'admin_head', 'fb_move_admin_bar' );
	// on frontend area
	add_action( 'wp_footer', 'fb_move_admin_bar' );

	add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 50;' ), 20 );


	
	function the_breadcrumb() {
		$url = $_SERVER["REQUEST_URI"];
		$urlArray = array_slice(explode("/",$url), 0, -1);
		$urlCount	=	count($urlArray);
		// Set $dir to the first value
		$dir = array_shift($urlArray);
		$count=1;
		echo '<li><a href="/">Home</a></li>';
		foreach($urlArray as $key=>$text)
		{
			$count++;
			if ($text != "home")
			{
				if ($count == $urlCount)
				{
					$dir .= "/$text";
					echo ' <li><a href="'.$dir.'">'.get_the_title().'</a></li>';
				}else
				{
					$dir .= "/$text";
					echo ' <li><a href="'.$dir.'">' . ucwords(strtr($text, array('_' => ' ', '-' => ' '))) . '</a></li>';
				}
			}
		}
	}
	
	function custom_read_more()
	{
    	return '...';
	}
	function excerpt($limit)
	{
	    return wp_trim_words(get_the_excerpt(), $limit, custom_read_more());
	}
	
	// instead of print_r() user pre_print() to automatically add the <pre> tag around it.
    function pre_print($print,$hide)
    {
            echo '<pre class="'. ($hide == true ? "hidden" : "") .'">';
            print_r($print);
            echo '</pre>';
    }
    
    //check out http://www.advancedcustomfields.com/add-ons/options-page/ for more information
    if( function_exists('acf_add_options_page') )
    {
	
		acf_add_options_page(array(
			'page_title' 	=> 'Theme Settings',
			'menu_title'	=> 'Theme Settings',
			'menu_slug' 	=> 'theme-settings',
			'redirect'		=> false
		));
		
		//Remove if using the multiple Locations setup
		acf_add_options_sub_page(array(
			'page_title' 	=> 'Company',
			'menu_title'	=> 'Company',
			'parent_slug'	=> 'theme-settings',
		));		
	}
	
	
	
	//Add Stevens Support link to admin bar
	add_action('admin_bar_menu', 'add_toolbar_items', 100);
	function add_toolbar_items($admin_bar)
	{
		$admin_bar->add_menu( array(
			'id'    => 'stevens-support',
			'title' => 'Submit Support Ticket',
			'href'  => 'http://tickets.stevensinc.com/Tickets/New',
			'target'=> '_blank',
			'meta'  => array(
				'title' => __('Submit Support Ticket'),			
			),
		));
	}
	
	function login_styles() 
	{
	    wp_enqueue_style('custom-login', '/assets/css/login.css');
	}
	add_action('login_enqueue_scripts', 'login_styles');


	//Order button text change
	add_filter( 'woocommerce_order_button_text', 'woo_archive_custom_checkout_button_text' );    // 2.1 +
 
	function woo_archive_custom_checkout_button_text() {
	 
	        return __( 'Reserve in Store', 'woocommerce' );
	 
	}
	
	//Add to Cart button text change
	add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +
 
	function woo_custom_cart_button_text() {
	 
	        return __( 'Add to Reservation', 'woocommerce' );
	 
	}
	
	//Shop page button text change
	add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );    // 2.1 +
 
	function woo_archive_custom_cart_button_text() {
	 
	        return __( 'Reserve', 'woocommerce' );
	 
	}
	
	//Change Checkout button to Complete Reservation
	function woocommerce_button_proceed_to_checkout()
	{
       $checkout_url = WC()->cart->get_checkout_url();
       ?>
       <a href="<?php echo $checkout_url; ?>" class="checkout-button button alt wc-forward"><?php _e( 'Complete Reservation', 'woocommerce' ); ?></a>
       <?php
     }
	
	// Change "Default Sorting" to "Our sorting" on shop page and in WC Product Settings
	function change_sorting_names( $catalog_orderby ) {
	    $catalog_orderby = str_replace("Default sorting", "Default", $catalog_orderby);
	    $catalog_orderby = str_replace("Sort by price: low to high", "Price: Low to High", $catalog_orderby);
	    $catalog_orderby = str_replace("Sort by price: high to low", "Price: High to Low", $catalog_orderby);
	    return $catalog_orderby;
	}
	add_filter( 'woocommerce_catalog_orderby', 'change_sorting_names' );
	add_filter( 'woocommerce_default_catalog_orderby_options', 'change_sorting_names' );
?>