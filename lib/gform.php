<?php
add_filter("gform_pre_render", "populate_dropdown");

//Note: when changing drop down values, we also need to use the gform_admin_pre_render so that the right values are displayed when editing the entry.
add_filter("gform_admin_pre_render", "populate_dropdown");

//Note: this will allow for the labels to be used during the submission process in case values are enabled
add_filter('gform_pre_submission_filter', 'populate_dropdown');
function populate_dropdown($form){

    //only populating drop down for form id 5
    if($form["id"] != 4)
       return $form;

    //Reading posts for "Business" category;
    //$models = get_posts(array("post_type" => 'hk_models'));
    
    
    $posts = get_posts(
    	array(
    		"post_type" 	=> 'rental', 
    		'posts_per_page' => -1, 
    		'post_status' 	=> 'publish',
			'order'			=> 'ASC',
			'orderby'		=> 'name',
		)
    );

    //Creating drop down item array.
    $items = array();

    //Adding initial blank value.
    $items[] = array("text" => "", "value" => "");

    //Adding post titles to the items array
    foreach($posts as $post)
        $items[] = array("value" => $post->post_name, "text" => $post->post_title);

    //Adding items to field id 8. Replace 8 with your actual field id. You can get the field id by looking at the input name in the markup.
    foreach($form["fields"] as &$field)
        if($field["id"] == 1)
        {            
            $field["choices"] = $items;
        }

    return $form;
}
?>