<?php
/**
* Menus

*/


  // Register wp_nav_menu() menus (http://codex.wordpress.org/Function_Reference/register_nav_menus)
  register_nav_menus(array(
    'top_navigation' => __('Top Navigation', 'roots'),
    'primary_navigation' => __('Primary Navigation', 'roots'),
    'sidebar_navigation' => __('Side Bar Navigation', 'roots'),
    'footer_navigation' => __('Footer Navigation', 'roots'),

  ));