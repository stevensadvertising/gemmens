<?php
/*

vc_map(
	array(
		"name" => __("Pull Quote", "js_composer"),
		"base" => "pullquote",
		"category" => __('Content', "js_composer"),
		"icon"		=> "",
		"params" => array(
			array(
				"type" => "textarea",
				"holder" => "",
				"class" => "",
				"heading" => __("Quote", "js_composer"),
				"param_name" => "content",
				"value" => __("", "js_composer")
			)
		)
	)
);
vc_map(
	array(
		"name" => __("Panel", "js_composer"),
		"base" => "panel",
		"icon"		=> "",
		"category" => __('Content', "js_composer"),
		"params" => array(
			array(
				"type" => "textfield",
				"holder" => "",
				"class" => "",
				"heading" => __("Heading", "js_composer"),
				"param_name" => "panel_heading",
				"value" => __("", "js_composer")
			),
			array(
				"type" => "textarea_html",
				"holder" => "<div>",
				"class" => "copy",
				"heading" => __("Copy", "js_composer"),
				"param_name" => "panel_content",
				"value" => __("", "js_composer")
			),
			array(
				"type" => "textfield",
				"holder" => "",
				"class" => "",
				"heading" => __("Footer", "js_composer"),
				"param_name" => "panel_footer",
				"value" => __("", "js_composer")
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Panel Type", "js_composer"),
				"param_name" => "panel_type",
				"value" => array(
					__("Default", "js_composer") => "default",
					__("Success", "js_composer") => "success",
					__("Info", "js_composer") => "info",
					__("Warning", "js_composer") => "warning",
					__("Danger", "js_composer") => "danger"
				),
				"description" => __("Select your panel style", "js_composer")
			)
		)//params
	)//panel
);
*/


/* Accordion block
---------------------------------------------------------- */
/*
vc_map( array(
	  "name" => __("Accordion"),
	  "base" => "accordion",
	  "show_settings_on_create" => false,
	  "is_container" => true,
	  "icon" => "icon-wpb-ui-accordion",
	  "category" => __('Content'),
	  "description" => __('Bootstrap Accordion'),
	  "params" => array(
		array(
			"type" => "textfield",
			"heading" => __("Widget title"),
			"param_name" => "title",
			"description" => __("What text use as a widget title. Leave blank if no title is needed.")
		),
		array(
			"type" => "textfield",
			"heading" => __("Active tab"),
			"param_name" => "active_tab",
			"description" => __("Enter tab number to be active on load or enter false to collapse all tabs.")
		),
		array(
			"type" => 'checkbox',
			"heading" => __("Allow collapsible all"),
			"param_name" => "collapsible",
			"description" => __("Select checkbox to allow for all sections to be be collapsible."),
			"value" => Array(__("Allow") => 'yes')
		),
		array(
			"type" => "textfield",
			"heading" => __("Extra class name"),
			"param_name" => "el_class",
			"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.")
		)
	  ),
	  "custom_markup" => '
	  <div class="wpb_accordion_holder wpb_holder clearfix vc_container_for_children">
	  %content%
	  </div>
	  <div class="tab_controls">
	  <button class="add_tab" title="'.__("Add accordion section").'">'.__("Add accordion section").'</button>
	  </div>
	  ',
	  'default_content' => '
	  [accordion_tab title="'.__('Section 1').'"][/accordion_tab]
	  [accordion_tab title="'.__('Section 2').'"][/accordion_tab]
	  '
	) );
	vc_map( array(
	  "name" => __("Accordion Section"),
	  "base" => "vc_accordion_tab",
	  "allowed_container_element" => 'vc_row',
	  "is_container" => true,
	  "content_element" => false,
	  "params" => array(
	    array(
	      "type" => "textfield",
	      "heading" => __("Title"),
	      "param_name" => "title",
	      "description" => __("Accordion section title.")
	    ),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Panel Type"),
			"param_name" => "type",
			"value" => array(
				"Default" => "default",
				"Success" => "success",
				"Info" => "info",
				"Warning" => "warning",
				"Danger" => "danger"
			),
			"description" => __("Select your panel style")
		)
	  ),
	  'js_view' => 'VcAccordionTabView'
	  ) 
);
*/

/*
EXAMPLE

http://kb.wpbakery.com/index.php?title=Visual_Composer_tutorial
http://kb.wpbakery.com/index.php?title=Vc_map

vc_map( 
	array(
		"name" => __("Bar tag test"),
		"base" => "bartag",
		"class" => "",
		"category" => __('Content'),
		'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
		'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
		"params" => array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __("Text"),
				"param_name" => "foo",
				"value" => __("Default params value"),
				"description" => __("Description for foo param.")
			)
		)
	) 
);
*/