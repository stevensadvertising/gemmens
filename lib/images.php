<?php
/**
* Image Sizes

*/

// Add post thumbnails (http://codex.wordpress.org/Post_Thumbnails)
add_theme_support('post-thumbnails');


//add_image_size('circular-image', 240, 240);  

// set_post_thumbnail_size(150, 150, false);
// add_image_size('category-thumb', 300, 9999); // 300px wide (and unlimited height)

// Add post formats (http://codex.wordpress.org/Post_Formats)
// add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));