<div class="container">
	<?php
	/**
	 * The Template for displaying all single products.
	 *
	 * Override this template by copying it to yourtheme/woocommerce/single-product.php
	 *
	 * @author 		WooThemes
	 * @package 	WooCommerce/Templates
	 * @version     1.6.4
	 */
	
	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}
	
	//get_header( 'shop' ); ?>
		<div class="row back-button-row">
			<div class="col-xs-24 back-button-container">
				<a href="/reserve-online/" class="button back-button">
					Back
				</a>
			</div>
		</div>
		<?php
			/**
			 * woocommerce_before_main_content hook
			 *
			 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			 * @hooked woocommerce_breadcrumb - 20
			 */
			//do_action( 'woocommerce_before_main_content' );
		?>
	
			<?php while ( have_posts() ) : the_post(); ?>
				
				<?php do_action( 'woocommerce_before_main_content' ); ?>
				<?php wc_get_template_part( 'content', 'single-product' ); ?>
	
			<?php endwhile; // end of the loop. ?>
	
		<?php
			/**
			 * woocommerce_after_main_content hook
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			do_action( 'woocommerce_after_main_content' );
		?>
	
		<?php
			/**
			 * woocommerce_sidebar hook
			 *
			 * @hooked woocommerce_get_sidebar - 10
			 */
			//do_action( 'woocommerce_sidebar' );
		?>
	
	<?php // get_footer( 'shop' ); ?>
</div>
