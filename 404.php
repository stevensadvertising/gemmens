<?php get_template_part('templates/content', 'page'); ?>
<div class="container error-page">
	<h1>404: Page Not found</h1>
	<div class="page-404">
		It seems that page you're looking for isn't here. Try doing something different!
	</div>
</div>