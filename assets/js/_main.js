// Modified http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
// Only fires on body class (working off strictly WordPress body_class)

var ExampleSite =
{
	// All pages
	common:
	{
		init: function()
		{
			$(document).ready(function()
			{
				/*
$('.header .dropdown .yamm-content').on('mouseover', function()
				{
					$(this).on('mouseleave',hideDropdown);
				});

				function hideDropdown(e)
				{
					$(this).closest('.open').removeClass('open');
					$(this).off('mouseleave');
				}
*/
			});
		}
	}
};


var UTIL = {
  fire: function(func, funcname, args)
  {
    var namespace = ExampleSite;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function')
    {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function()
  {

    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm)
    {
      UTIL.fire(classnm);
    });

    UTIL.fire('common', 'finalize');
  }
};

$(document).ready(UTIL.loadEvents);



$(document).ready(function(){
	//add triangles to footer
	$('.gform_footer').prepend('<div class="button-tri"></div>');

	
	var $results=$('.rental-product'),
    $checks=$('.rental-filter-list :checkbox');

	$checks.change(function()
	{
	    var $checked=$checks.filter(':checked');
	    
	    /* show all when nothing checked*/
	    if(!$checked.length){
	        $results.show();
	        return; /* quit here if nothing checked */
	    }
	    
	    /* create array of checked values */
	    var checkedVals= $.map($checked, function(el){
	        return el.value;
	    });
	    
	    /* hide all results, then filter for matches */
	    $results.hide().filter(function()
	    {
	        /* split categories for this result into an array*/
	        var cats=$(this).data('filter').split(' ');
	        /* filter the checkedVals array to only values that match */
	        var checkMatches=$.grep(checkedVals, function(val){              
	            return $.inArray(val, cats) >-1;
	        });
	        /* only return elements with matched array and original array being same length */             
	        return checkMatches.length === checkedVals.length;
	     /* show results that match all the checked checkboxes */            
	    }).show();
	    
	    /* do something when there aren't any matches */
	    if(!$results.length){
	       alert('Ooops...no matches');
	    }
	
	
	});
	
	//match homepage boxes
	$('.deco-section').matchHeight();
	$('.equal').matchHeight();
	
	// Match Menu heights
	$('#menu-primary-navigation li a').matchHeight();
	
	//match product boxes
	$('.product').matchHeight();
	
	//set carousel height on load
	var $width = $( ".carousel-inner" ).outerWidth();
	$( ".item" ).css('height', $width/2.208); //divide by ratio of height to width
	
	
	//Cart Item Counter
	$ci = 0;
	
	//Add to Cart Item Counter if there's cart items
	$('.cart_item').each(function(){
		$ci++;
	});
	
	//if Cart Item Counter doesn't remain 0, inject the text. Otherwise, don't inject the text
	if($ci > 0)
	{		
		//inject text
		var $text 	 =  '<div class="col-xs-24 col-sm-24 reservation-warning-container>';
			$text	+=  '<h4 class="reservation-warning">Your reservation will be confirmed by either phone or email. <strong>Item must be paid for in the store within 24 hours of confirmation</strong> or it will be placed back into inventory.</h4>';
			$text	+=  '</div>';
		$($text).insertBefore('#payment');
		$($text).insertBefore('.container .woocommerce');
	}
	
	//Replace read more with out of stock text on product archive page
	$('.products .product.outofstock').find('.button').html('Learn more');
	
	//Remove Billing Details
	var $text = $('.woocommerce-billing-fields h3').text();
	var $element = $('.woocommerce-billing-fields h3');
	
	if($text == 'Billing Details')
	{
		$($element).remove();
	}
	
	//Make Rental product dropdown read-only
	$('.rental-product-select .gfield_select').prop('disabled', true);

});

$( window ).resize(function()
{
	//set carousel height on resize
	var $width = $( ".carousel-inner" ).outerWidth();
	$( ".item" ).css('height', $width/2.208); //divide by ratio of height to width
});