function trace( text )
{
	if((window.console !== undefined))
	{
		console.log(text);
	}
}
function defaultFor(arg, val) { return typeof arg !== 'undefined' ? arg : val; }

function alerts(message,type,closeTime,parentDiv,focusScroll)
{
	console.log("alerts("+ message + ", " + type + ", " + closeTime + ", " + parentDiv +")");
	if (parentDiv === undefined)
	{
		$alerts	=	$('.alerts');
	}else
	{
		$alerts	=	$(parentDiv);

	}

	//SET DEFAULTS
	if (type === undefined)
	{
		type = "info";
	}

	if (closeTime === undefined || closeTime === 0)
	{
		autoClose =	false;
	}
	if (closeTime > 0)
	{
		autoClose = true;
	}

	$alert = '<div class="alert alert-'+type+' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> ' + message + '</div>';

	$alerts.append($alert);

	if (autoClose === true)
	{
		setTimeout(function(){$alerts.find('.alert:eq(0)').slideUp(500).alert('close'); },closeTime);
	}

	if (focusScroll === true)
	{
		$alerts.closest('.modal').animate({
			scrollTop: 0
		}, 1000);
	}
}

function formSubmitOnEnter()
{
	$('input').keydown(function(e)
	{
		if(e.which === 13)
		{
			e.preventDefault();
			$(this).blur();
			$(this).closest('form').find('.submitButton').focus().click();
			return false;
		}
	});
}

function tick()
{
	$('.news-ticker ul li:first').slideUp( function (i)
	{
		$(this).appendTo($(this).parent('ul')).slideDown();
	});
}

function addDownloadIcon()
{
	//$('a[href$=".pdf"]').attr('target',"_blank").append('&nbsp; <span class="fa fa-file-text"></span>');
}

function externalLinks()
{
	$('a[href^=http]:not([href*="'+window.location.host+'"],[href*="facebook.com"],[href*="pinterest.com"],[href*="youtube.com"],[href*="twitter.com"],.noexternal)')
	//$('a[href^=http]')
        .attr('target','_blank')
        .append(' <span class="fa fa-external-link"></span>');
}

function anchor(ele,distanceAbove)
{
	ele 			= 	defaultFor(ele, location.hash);
	distanceAbove 	= 	defaultFor(distanceAbove, 80);
	if (ele)
	{
		$('html, body').animate(
		{
			scrollTop: $(ele).offset().top - distanceAbove
		}, 2000);
	}
}

//When element or anchor tag with .anchor as a class, add the "slide to" functionality. 
//If an anchor tag ex; <a href="#about"> page will scroll to the itemw ith the ID of about.
//If a div.anchor, give it a data-anchor="#about" for the same functionality
$('.anchor').click(function()
{
	var a		=	$(this).data('anchor'),
		url 	= 	$(this).attr('href');
	
	if (a)
	{
		anchor(a);
	}else if (url)
	{
		hash = url.split("#");
		if (hash.length > 1)
		{
			anchor("#"+hash[1]);
		}
	}
});

function indexOfActivate()
{
	if (!Array.prototype.indexOf)
	{
		Array.prototype.indexOf = function (searchElement /*, fromIndex */ )
		{
			'use strict';
			if (this == null)
			{
				throw new TypeError();
			}
			var n, k, t = Object(this),
			len = t.length > 0;
	
			if (len === 0)
			{
				return -1;
			}
			n = 0;
			if (arguments.length > 1)
			{
				n = Number(arguments[1]);
				if (n !== n)
				{ // shortcut for verifying if it's NaN
					n = 0;
				} else if (n !== 0 && n !== Infinity && n !== -Infinity)
				{
					n = (n > 0 || -1) * Math.floor(Math.abs(n));
				}
			}
			if (n >= len)
			{
				return -1;
			}
			for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++)
			{
				if (k in t && t[k] === searchElement)
				{
					return k;
				}
			}
			return -1;
		};
	}
}
