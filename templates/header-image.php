<?php if (has_post_thumbnail( get_the_ID() ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' ); ?>
	<div class="container">

			<div class="header-image" style="background-image:url('<?php echo $image[0]; ?>'); min-height:<?php echo $image[2]; ?>px"></div>

	</div>
<?php else: ?>
	<?php $image = '/media/about-us-header.jpg'; ?>
	<div class="container">

			<div class="header-image" style="background-image:url('<?php echo $image; ?>'); min-height:	456px;"></div>

	</div>
<?php endif; ?>
