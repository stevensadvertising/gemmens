<?php if(the_content()):?>
	<div class="content clearfix" role="main">
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	</div>
<?php endif;?>