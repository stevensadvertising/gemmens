<?php 
	$footer_image 		= get_field('footer_logo', 'option');
	$footer_logo 		= get_field('right_side_logo', 'option');
	$right_footer_link 	= get_field('right_footer_link', 'option');
	$signup 			= get_field('sign-up_text', 'option');
	$fblink 			= get_field('facebook_link', 'option');
	$fbtext 			= get_field('facebook_text', 'option');
	$pricematch 		= get_field('price_match', 'option');
	$employee	 		= get_field('employee_portal', 'option');
?>

<footer class="footer clearfix">
	<div class="row trans-footer">
		<div class="container">
			
			<div class="col-xs-24 col-sm-6 signup-text">
				<?=$signup?>
			</div>
			
			<div class="col-xs-24 col-sm-9 email-bar">
				<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
			</div>
			
			<div class="col-xs-18 col-sm-7 facebook">
				<div class="col-xs-20 facebook-text">
					<?=$fbtext?>
				</div>
				<div class="col-xs-4 facebook-icon">
					<a href="<?=$fblink?>" class="facebook noexternal"><img src="/assets/img/facebook.png"></a>
				</div>
			</div>
			
			<div class="col-xs-6 col-sm-2 pricematch">
				<img src="<?=$pricematch[url]?>" />
			</div>
		</div>
	</div>
	<div class="row brown-footer">
		<div class="container">
			<div class="col-xs-24 col-sm-4 footer-logo">
				<img src="<?=$footer_image[url]?>" />
			</div>
			<div class="col-xs-24 col-sm-16">
				<div class="deco-store-information">
					<?php
					if( have_rows('first_store_information', 'option') ):
					?>
						
						<ul class="deco-store-information-list first">
						
							<?php
							while ( have_rows('first_store_information', 'option') ) : the_row();
								$info = get_sub_field('information', 'option');
								$class = get_sub_field('class', 'option');
							?>
							
								<li class="<?=$class; ?>">
									<?=$info ?>
								</li>
							
							<?php endwhile; ?>
						</ul>
						
					<?php endif; ?>
					
					<?php
					if( have_rows('second_store_information', 'option') ):
					?>
						
						<ul class="deco-store-information-list second">
						
							<?php
							while ( have_rows('second_store_information', 'option') ) : the_row();
								$info = get_sub_field('information', 'option');
								$class = get_sub_field('class', 'option');
							?>
							
								<li class="<?=$class; ?>">
									<?=$info ?>
								</li>
							
							<?php endwhile; ?>
						</ul>
						
					<?php endif; ?>
				</div>
			</div>
			<div class="col-xs-24 col-sm-4 footer-logo">
				<a href="<?=$right_footer_link?>" class="noexternal">
					<img src="<?=$footer_logo[url]?>" />
				</a>
			</div>
			<div class="employee-login">
				<a href="<?=$employee?>"></a>
			</div>
		</div>
	</div>
</footer>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69817006-1', 'auto');
  ga('send', 'pageview');

</script>
<?php wp_footer(); ?>