<div class="content clearfix rental-content-page" role="main">
	<div class="container">
		
		
		<div class="visible-xs col-xs-24 col-sm-8 single-rental-container">
			<div class="rental-filter-container">
				<h3>Filter</h3>
				<ul class="rental-filter-list">
					
					<li class="rental-filter" data-filterby="all">
						<span class="count"></span>
						<input type="checkbox" value="all" id="filter-all" <?php if ($_GET['filter'] == 'all'){ echo "checked";} ?> />
						<label for="filter-all">All</label>
					</li>
					<?php
						$arg = array(
						    'hide_empty'        => false 
						); 
						$terms = get_terms( 'rentalcategory', $arg );
						
						
							foreach ( $terms as $term ) :?>

								<li class="rental-filter" data-category="<?=$term->slug;?>">
									<span class="count"><?=$term->count;?></span>
									
									
									<input type="checkbox" value="<?=$term->slug;?>" id="filter-<?=$term->slug;?>" <?php if ($_GET['filter'] == $term->slug){ echo "checked";} ?> />
									<label for="filter-<?=$term->slug;?>"><?=$term->name;?></label>
								</li>
							
							<?php endforeach;
						
						//endif;
					?>	
				</ul>
			</div>
			
			<div class="single-rental-form">
				<?php //the_content(); ?>
				<?php // echo do_shortcode('[gravityform id="4" title="true" description="false"]'); ?>
			</div>
		</div>

		<div class="col-xs-24 col-sm-16">
		<?php while (have_posts()) : the_post(); ?>
			<?php if (!is_front_page() ): ?><h1><?php the_title(); ?></h1><?php endif; ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
		
		<?php 
		$limit = $limit ?: -1;
		$offset = $offset ?: 'offset';
		$category = $category ?: '';
		$category_name = $category_name ?: '';
		$orderby = $orderby ?: 'title';
		$order = $order ?: 'DESC';
		$include = $include ?: '';
		$exclude = $exclude ?: '';
		$meta_key = $meta_key ?: '';
		$meta_value = $meta_value ?: '';
		$post_type = $post_type ?: 'post';
		$post_mime_type = $post_mime_type ?: '';
		$post_parent = $post_parent ?: '';
		$post_status = $post_status ?: 'publish';
		$suppress_filters = $suppress_filters ?:  true;
		$location = $location ?: false;
		
		$classes = $classes ?: 'col-xs-24 col-sm-12';
			
		
		$args = array(
			'posts_per_page'   => $limit,
			'offset'           => $offset,
			'category'         => $category,
			'category_name'    => $category_name,
			'orderby'          => 'title',
			'order'            => 'ASC',
			'include'          => $include,
			'exclude'          => $exclude,
			'meta_key'         => $meta_key,
			'meta_value'       => $meta_value,
			'post_type'        => 'rental',
			'post_mime_type'   => $post_mime_type,
			'post_parent'      => $post_parent,
			'post_status'      => $post_status,
			'suppress_filters' => $suppress_filters ); 
			 /* col-sm- echo $columns; if ($columnsOffset){ echo 'col-sm-offset-'.$columnsOffset;} */
			
		?>
		<ul class="rentals-list">
			<?php
			global $post;
			$myposts = get_posts( $args );
			foreach ( $myposts as $post ) : 
				setup_postdata( $post ); ?>
				<?php
					$rc = wp_get_object_terms( $post->ID, 'rentalcategory');
					$rcat = $rc;
					
					$rcat_string = '';
					foreach($rcat as $rcat)
					{
						$rcat_string .= $rcat->slug . ' ';
					}
					
				?>
				<li class="col-xs-24 rental-product <?=$rcat_string?> all" data-filter="<?=$rcat_string?> all">
					<h3 class="rental-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				</li>
			<?php endforeach;
			wp_reset_postdata(); ?>
		</ul>
		
		
		</div>
		
		<div class="hidden-xs col-sm-8 single-rental-container">
			<div class="rental-filter-container">
				<h3>Filter</h3>
				<ul class="rental-filter-list">
					
					<li class="rental-filter" data-filterby="all">
						<span class="count"></span>
						<input type="checkbox" value="all" id="filter-all" <?php if ($_GET['filter'] == 'all'){ echo "checked";} ?> />
						<label for="filter-all">All</label>
					</li>
					<?php
						$arg = array(
						    'hide_empty'        => false 
						); 
						$terms = get_terms( 'rentalcategory', $arg );
						
						
							foreach ( $terms as $term ) :?>

								<li class="rental-filter" data-filterby="<?=$term->slug;?>">
									<span class="count"><?=$term->count;?></span>
									
									
									<input type="checkbox" value="<?=$term->slug;?>" id="filter-<?=$term->slug;?>" <?php if ($_GET['filter'] == $term->slug){ echo "checked";} ?> />
									<label for="filter-<?=$term->slug;?>"><?=$term->name;?></label>
								</li>
							
							<?php endforeach;
						
						//endif;
					?>	
				</ul>
			</div>
			
			<div class="single-rental-form">
				<?php //the_content(); ?>
				<?php // echo do_shortcode('[gravityform id="4" title="true" description="false"]'); ?>
			</div>
		</div>
	</div>
</div>