<?php $image = get_field('company_logo','option'); ?>
<?php $tagline = get_field('tagline','option'); ?>

<div class="header clearfix">
	<nav class="navbar navbar-default" role="navigation">
		<div class="white-header">
			<div class="container">
				<div class="col-xs-11 col-xs-offset-11 col-sm-10 col-sm-offset-5 col-md-10 col-md-offset-7 col-lg-10 col-lg-offset-6 tagline">
					Local - Knowledge - Solutions
				</div>
				<div class="col-xs-24 col-sm-8 col-md-7 pull-right hidden-xs">
					<?php
					if (has_nav_menu('top_navigation'))  :
						wp_nav_menu(array('theme_location' => 'top_navigation', 'menu_class' => 'top-nav nav nav-stacked', 'link_before' => '<div class="brown"><div class="brown-inner"></div></div>'));
					endif;
					?>
				</div>
			</div>
		</div>
		<div class="container grey-header">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".header-nav">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php bloginfo('url'); ?>">
					<img src="<?=$image?>" class="noexternal" />
				</a>
			</div>
			<div class="collapse navbar-collapse header-nav clearfix">
				<div class="col-xs-24 col-sm-8 col-md-6 visible-xs">
					<?php
					if (has_nav_menu('top_navigation'))  :
						wp_nav_menu(array('theme_location' => 'top_navigation', 'menu_class' => 'top-nav nav nav-stacked', 'link_before' => '<div class="brown"><div class="brown-inner"></div></div>'));
					endif;
					?>
				</div>
				
				<?php
				if (has_nav_menu('primary_navigation'))  :
					wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav nav-pills', 'link_before' => '<div class="dropdown-grey hidden-xs"><div class="dropdown-grey-inner"></div></div>'));
				endif;
				?>
			</div>
		</div>
	</nav>
</div>