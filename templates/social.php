<ul class="social">
<?php
	$social	=	get_field('social','option');
	while(has_sub_field('social', 'option')):
		$type	=	get_sub_field('type');
		if ($type == "Image")
		{
			$image	=	get_sub_field('image'); 
			$icon 	=	'<img src="'.$image['url'].'" />';
		}
		if ($type == "Font Icon")
		{ 
			$social_icon	=	get_sub_field('icon');	
			if ($social_icon == "other")
			{
				$icon	=	'<span class="fa '. get_sub_field('font_awesome_code').'"></span>';
			}else
			{
				$icon	=	'<span class="fa '. $social_icon.'"></span>';
			}
		}
		$url	=	get_sub_field('url');
		?>
		<li><a href="<?=$url;?>" target="_blank"><?= $icon; ?></a></li>
		<?	
	endwhile;
	?>
</ul>
