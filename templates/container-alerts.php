<?php
	$tacos = null;
	
	if($tacos == 'volcano') :
	$params = array(
		'limit' => -1,
		'post_type' => 'alert',
		//Include this if using multiple locations
		/*'meta_query' => array(
							array(
								'key' => 'location', // name of custom field
								'value' => '"117"', // matches exaclty "123", not just 123. This prevents a match for "1234"
								'compare' => 'LIKE'
							)
						)	
						*/
	);

/*
USE if multiple Locations
if (isset($loc_ID))
{
	$params = array(
		'limit' => -1,
		'post_type' => 'alert',
		'meta_query' => array(
							array(
								'key' => 'location', // name of custom field
								'value' => '"' . $loc_ID . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
								'compare' => 'LIKE'
							)
						)	
	);
	//array_push($params,$media_query);
}
*/
	global $post;
	$alerts = get_posts( $params );
	
?>
<? //Keep if using jquery alerts ?>
<ul class="alerts">
<?php
	if (count($alerts) != 0)
	{
	
	foreach ( $alerts as $post ) : 
		setup_postdata( $post );
		$post_ID = get_the_ID();
		
			if (get_field('type', $post_ID) == "success")	{	$icon	=	'exclamation';}
			if (get_field('type', $post_ID) == "warning")	{	$icon	=	'exclamation-triangle';}
			if (get_field('type', $post_ID) == "danger")	{	$icon	=	'ban';}
			if (get_field('type', $post_ID) == "info")		{	$icon	=	'exclamation-circle';}
		?>
		<li class="alert alert-<?php the_field('type', $post_ID); ?>">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<span class="fa fa-<?php echo $icon; ?> pull-left"></span>
			<?php the_content(); ?>
		</li>
		
	<?php endforeach; ?>
<? } wp_reset_postdata(); ?>
</ul>

<?php endif; ?>