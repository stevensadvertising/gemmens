<?php $alert = get_field('holidays', 'option'); ?>
<?php $type = get_field('alert_type', $alert[0]->ID); ?>
<?php if($alert) :?>
	<div class="alert alert-gemmens alert-<?=$type?>">
		<?php echo apply_filters('the_content', $alert[0]->post_content); ?>
	</div>
<?php endif; ?>