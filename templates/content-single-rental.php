<?php
	$slug = $post->post_name;
?>

<div class="content clearfix single-rental-container" role="main">
	<div class="container">
		<div class="row back-button-row">
			<div class="col-xs-24 back-button-container">
				<a href="/rentals/" class="button back-button">
					Back to rentals
				</a>
			</div>
		</div>
		
		<?php while (have_posts()) : the_post(); ?>
			<article <?php post_class(); ?>>
				<div class="row">
					<div class="col-xs-24 col-sm-16">
						<?php if (has_post_thumbnail( get_the_ID() ) ): ?>
							<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' ); ?>
							<img class="rental-image" src="<?=$image[0]; ?>" alt="<?=$slug;?>" />
						<?php else : ?>
							<img class="rental-image hidden" src="/assets/img/placeholder_large.jpg" alt="<?=$slug;?>" />
						<?php endif; ?>
						
						<div class="rental-content">
						<h1 class="rental-header"><?php the_title(); ?></h1>
						<?php the_content(); ?>
						</div>
					</div>
					
					
					<div class="col-xs-24 col-sm-8">
						<div class="single-rental-form">
							
							<?php echo do_shortcode('[gravityform id=4 tabindex=32 field_values="p='.$slug.'"]'); ?>
						</div>
					</div>
				</div>
				
				
			</article>
		<?php endwhile; ?>
	</div>
</div>