<div class="content clearfix" role="main">
	<div class="container brown-background">
		<?php while (have_posts()) : the_post(); ?>
			<?php if (!is_front_page() ): ?><h1 class="header-h1"><?php the_title(); ?></h1><?php endif; ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	</div>
</div>