<?php 
	$params = array( 
		'limit' => 3
	);
	
	$newsItems = pods( 'news', $params );
?>	

<div class="news-ticker">
	<ul>
    	<?php while ( $newsItems->fetch() ) : ?>
		    <li>
		    	<a href="/news/<?php echo $newsItems->display('slug'); ?>">
					<p><?php echo $newsItems->field('post_title'); ?></p>
		    	</a>
			</li>
		<?php endwhile; ?>
	</ul>
</div>

<script type="text/javascript">
	setInterval(function(){ tick (); }, 7000);
</script>