<?php 
	$image = get_field('store_image', 'option');
	$map = get_field('store_map', 'option');
	$open = get_field('open_sign', 'option');
	$closed = get_field('closed_sign', 'option');
	$shop_online_image = get_field('shop_online_image', 'option');
	$shop_online_link = get_field('shop_online_link', 'option');
	$hours = get_field('actual_hours', 'option');
	$today = date('w');
	$opent = $hours[$today][open];
	$closet = $hours[$today][close];
	$now = current_time( 'Hi', 0 );
?>

<div class="content clearfix home-page" role="main">
	<div class="container">
		<div class="hours-location deco-section col-xs-24 col-sm-13">
			<div class="deco-header row">
				Hours & Location <?php echo $time?>
			</div>
			<div class="row">
				<div class="deco-store-sign col-xs-24 col-sm-5">
					<?php if (($opent <= $now) && ($now <= $closet)): ?>
						<div class="deco-store open">
							<?php echo $open?>
							
						</div>
					<?php else :?>
						<div class="deco-store closed">
							<?php echo $closed?>
						</div>
					<?php endif;?>
					<div class="deco-tri">
					</div>
				</div>
				
				<div class="deco-store-information col-xs-24 col-sm-15 col-sm-offset-1 col-md-17 col-md-offset-1">
					<?php
					if( have_rows('first_store_information', 'option') ):
					?>
						
						<ul class="deco-store-information-list first">
						
							<?php
							while ( have_rows('first_store_information', 'option') ) : the_row();
								$info = get_sub_field('information', 'option');
								$class = get_sub_field('class', 'option');
							?>
							
								<li class="<?php echo $class; ?>">
									<?php echo $info ?>
								</li>
							
							<?php endwhile; ?>
						</ul>
						
					<?php endif; ?>
					
					<?php
					if( have_rows('second_store_information', 'option') ):
					?>
						
						<ul class="deco-store-information-list second">
						
							<?php
							while ( have_rows('second_store_information', 'option') ) : the_row();
								$info = get_sub_field('information', 'option');
								$class = get_sub_field('class', 'option');
							?>
							
								<li class="<?php echo $class; ?>">
									<?php echo $info ?>
								</li>
							
							<?php endwhile; ?>
						</ul>
						
					<?php endif; ?>
				</div>
			</div>
			
			<div class="row">
				<div class="store-image col-xs-24 col-sm-12">
					<img src="<?php echo $image[sizes][medium]?>" />
				</div>
				
				<div class="store-map col-xs-24 col-sm-12" >
					<?php echo $map?>
				</div>
			</div>
		</div>
		
		<div class="shop-online deco-section col-xs-24 col-sm-10 col-sm-offset-1">
			<div class="deco-header row">
				Shop Online <!--Shop Online-->
			</div>
			
			<div class="shop-online-image">
				<img src="<?php echo $shop_online_image[url]?>" />
				<!-- <p class="shop-online-headline">Start your spring landscaping!</p> -->
				<a href="<?php echo $shop_online_link?>" class="button noexternal">Get Started</a>
			</div>
		</div>
		<?php while (have_posts()) : the_post(); ?>
			<?php if (!is_front_page() ): ?><h1><?php the_title(); ?></h1><?php endif; ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	</div>
</div>