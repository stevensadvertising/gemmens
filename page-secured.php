<?php
/*
Template Name: Secured Page
*/
?>



<?php
  $islogged = "false";
  if(isset($GLOBALS['pc_user_id'])) {
    $islogged = "true";
  } else {
      // no user logged
  }
?>

<?php if($islogged == 'false') :?>

	<div class="container login-form secure-not-logged-in">
		<div class="col-xs-24 col-sm-16 col-sm-offset-4">
			<div class="disclaimer">
				Sorry this page is secured, please contact administration if you feel you should be able to access this page
			</div>
			<div class="form">
				<?php echo do_shortcode('[pc-login-form]'); ?>
			</div>
		</div>
	</div>

<?php elseif($islogged == 'true') :?>

	<?php $schedule = get_field('schedule'); ?>
	<?php $handbook = get_field('handbook'); ?>

	<div class="container secure-logged-in sidebar-template">
		<div class="col-xs-24 col-sm-8 col-md-6 sidebar">
			<div class="profile-container">
				<h1 class="green marker col-xs-24">Profile</h1>
				<div class="pc-name col-xs-24">
					<?=pg_user_logged()->name?>
				</div>

				<div class="col-xs-24">

<!-- 					<a class="pc-profile" href="/profile">Profile</a> -->
					<?php echo do_shortcode('[pc-logout-box]'); ?>
				</div>
			</div>

			<?php
				if (has_nav_menu('secured_navigation'))  :
					wp_nav_menu(array('theme_location' => 'secured_navigation', 'menu_class' => 'nav nav-stacked'));
				endif;
			?>

		</div>
		<div class="col-xs-24 col-sm-16 col-md-18 overflow-hidden">
			<?php get_template_part('templates/content', 'page-secure'); ?>

			<!--
			<?php if($schedule) :?>
				<h2>Schedule</h2>
				<a class="pc-profile" href="<?=$schedule[url]?>">View Schedule</a>
				<a class="pc-profile" href="<?=$schedule[url]?>" download="<?=$handbook[url]?>">Download Schedule</a>
			<?php endif;?>
	-->


			<?php if( have_rows('schedule_multiple') ): ?>
				<h2>Schedules</h2>

				<?php while ( have_rows('schedule_multiple') ) : the_row(); ?>

					<?php
						$today = date(Ymd);
						$expireDate = get_sub_field('expire_date');
					?>

					<?php if ($expireDate >= $today) : ?>

					<div>
						<a class="pc-profile" href="<?php the_sub_field('file'); ?>">View Schedule: <?php the_sub_field('name'); ?></a>
						<a class="pc-profile" href="<?php the_sub_field('file'); ?>" download="<?php the_sub_field('file'); ?>">Download Schedule: <?php the_sub_field('name'); ?></a>
					</div>

					<?php endif ?>

			<?php endwhile; endif; ?>



			<?php if($handbook) :?>
				<h2>Employee Handbook</h2>
				<a class="pc-profile" href="<?=$handbook[url]?>">View Handbook</a>
				<a class="pc-profile" href="<?=$handbook[url]?>" download="<?=$handbook[url]?>">Download Handbook</a>
			<?php endif;?>
		</div>
	</div>

<?php endif; ?>