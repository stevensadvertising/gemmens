<?php
	global $post;
	if (!$idname)	{	$idname		=	'carousel' . generateRandomString(); }
	if (!$interval)	{	$interval	=	'8000'; 	}
	if (!$pod)		{	$pod		=	'slide';	}
	if (!$limit)	{	$limit		=	-1;	}

	$args = array(
		'post_type' 		=> $pod,
		'posts_per_page' 	=> $limit,
		'order'				=> 'DESC',
		'orderby'			=> 'menu_order',
		'post_status'       => 'publish'
	);

	$slides 	=	get_posts( $args );
	$slideCount	=	count($slides);
?>

<section class="gallery">
	<div id="<?=$idname;?>" class="carousel slide" <?php if ($slideCount > 1) { ?> data-ride="carousel" data-interval="<?php echo $interval;?>" <?php } ?> data-category="">
		 <?php if ($slideCount > 1) { ?>
			<ol class="carousel-indicators">
				<?php
				$a = 0;
		        foreach ( $slides as $slide )
		        {
		            $a++;
		        	?>
					<li data-target="#<?=$idname;?>" data-slide-to="<?php echo ($a-1); ?>" class="<?php if ($a == 1) : ?> active <?php endif; ?>"><span></span></li>
				<? } ?>
			</ol>
		<?php } ?>

		<div class="carousel-inner">
			<?php
			$i = 0;
	        foreach ( $slides as $post )
	        {
		        setup_postdata( $post );
	            $i++;
	            $link = get_field('link');
	            $mimage = get_field('mobile_slide');
	            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
				?>
				<div class="item <?php if ($i == 1) : ?> active <?php endif; ?>" <?php if (has_post_thumbnail( $post->ID ) ): ?>style="background-image:url(<?=$image[0]; ?>)" <?php endif; ?>>
					<a class="carousel-link noexternal" href="<?=$link?>" target="_blank">
						<?php if($mimage) :?>
							<img class="visible-xs" src="<?=$mimage[url]; ?>" />
						<?php endif;?>
					</a>
					
					<button class="btn btn-collapse visible-xs" type="button" data-toggle="collapse" data-target="#caption<?=$i?>" aria-expanded="false" aria-controls="caption<?=$i?>">
					  Info
					</button>
					
					<div id="caption<?=$i;?>"  class="caption collapse">
						<div class="container-caption">
							<div class="vc_row wpb_row row">
								<div class="wpb_column vc_column_container col-xs-24 col-sm-24 col-md-24 col-lg-24">
									<?php the_title(); ?>
								</div>
							</div>
						
							<?php the_content(); ?>
						</div>
					</div>
				</div>


			<?php
			}
			wp_reset_postdata();
			?>
		</div>
		<?php if ($slideCount > 1) { ?>
			<a class="left carousel-control" href="#<?=$idname;?>" data-slide="prev"><span class="fa fa-angle-left"></span><div class="gradient"></div></a>
			<a class="right carousel-control" href="#<?=$idname;?>" data-slide="next"><span class="fa fa-angle-right"></span><div class="gradient reverse"></div></a>
		<?php } ?>
	</div>
</section>