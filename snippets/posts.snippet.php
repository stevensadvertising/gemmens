<?php 
	$limit = $limit ?: -1;
	$offset = $offset ?: 'offset';
	$category = $category ?: '';
	$category_name = $category_name ?: '';
	$orderby = $orderby ?: 'post_date';
	$order = $order ?: 'DESC';
	$include = $include ?: '';
	$exclude = $exclude ?: '';
	$meta_key = $meta_key ?: '';
	$meta_value = $meta_value ?: '';
	$post_type = $post_type ?: 'post';
	$post_mime_type = $post_mime_type ?: '';
	$post_parent = $post_parent ?: '';
	$post_status = $post_status ?: 'publish';
	$suppress_filters = $suppress_filters ?:  true;
	$location = $location ?: false;
	
	$classes = $classes ?: 'col-xs-24 col-sm-12';
	

$args = array(
	'posts_per_page'   => $limit,
	'offset'           => $offset,
	'category'         => $category,
	'category_name'    => $category_name,
	'orderby'          => $orderby,
	'order'            => $order,
	'include'          => $include,
	'exclude'          => $exclude,
	'meta_key'         => $meta_key,
	'meta_value'       => $meta_value,
	'post_type'        => $post_type,
	'post_mime_type'   => $post_mime_type,
	'post_parent'      => $post_parent,
	'post_status'      => $post_status,
	'suppress_filters' => $suppress_filters ); 
	 /* col-sm- echo $columns; if ($columnsOffset){ echo 'col-sm-offset-'.$columnsOffset;} */
	
?>
<ul class="posts-list <?php if ($post_type == 'news'){ ?>news-items<? } ?>">
	<?php
	global $post;
	$myposts = get_posts( $args );
	foreach ( $myposts as $post ) : 
		setup_postdata( $post ); ?>
		<li class="<?php echo $classes; ?>">
			<h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<?php if ($post_type == 'news'){ ?><time class="published" datetime="<?php echo get_the_time('c'); ?>"><?php echo get_the_date(); ?></time><? } ?>
			<div class="post-excerpt">
				<?php the_excerpt(); ?>
			</div>
		</li>
	<?php endforeach;
	wp_reset_postdata(); ?>
</ul>