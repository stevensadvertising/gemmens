<?php 
	$limit = $limit ?: -1;
	$offset = $offset ?: 'offset';
	$category = $category ?: '';
	$category_name = $category_name ?: '';
	$orderby = $orderby ?: 'post_date';
	$order = $order ?: 'DESC';
	$include = $include ?: '';
	$exclude = $exclude ?: '';
	$meta_key = $meta_key ?: '';
	$meta_value = $meta_value ?: '';
	$post_type = $post_type ?: 'post';
	$post_mime_type = $post_mime_type ?: '';
	$post_parent = $post_parent ?: '';
	$post_status = $post_status ?: 'publish';
	$suppress_filters = $suppress_filters ?:  true;
	$location = $location ?: false;
	
	$classes = $classes ?: 'col-xs-24 col-sm-12';
	

$args = array(
	'posts_per_page'   => $limit,
	'offset'           => $offset,
	'category'         => $category,
	'category_name'    => $category_name,
	'orderby'          => $orderby,
	'order'            => $order,
	'include'          => $include,
	'exclude'          => $exclude,
	'meta_key'         => $meta_key,
	'meta_value'       => $meta_value,
	'post_type'        => 'coupon',
	'post_mime_type'   => $post_mime_type,
	'post_parent'      => $post_parent,
	'post_status'      => $post_status,
	'suppress_filters' => $suppress_filters ); 
	 /* col-sm- echo $columns; if ($columnsOffset){ echo 'col-sm-offset-'.$columnsOffset;} */
	
?>

<div class="home-page">
	<?php
	global $post;
	$myposts = get_posts( $args );
	foreach ( $myposts as $post ) : 
		setup_postdata( $post ); ?>
		
		<?php $file = get_field('coupon'); ?>
		<?php $thumb = get_field('thumbnail_image'); ?>

		<div class="deco-section equal coupon col-xs-24 col-sm-6">
			<?php if($file) :?>
				<?php if($file[type] == 'image') :?>
					<a href="<?=$file[url]?>" target="_blank">
						<div class="deco-header row">
							<?php the_title(); ?>
						</div>
						
						<?php if($thumb) :?>
							<img src="<?=$thumb[sizes][medium]?>" />
						<?php else: ?>
							<img src="<?=$file[sizes][medium]?>" />
						<?php endif; ?>
						
						<?php if(get_the_content()) :?>
							<div class="coupon-content">
								<?php the_content(); ?>
							</div>
						<?php endif; ?>
					</a>
				<?php endif; ?>
				
				<?php if($file[mime_type] == 'application/pdf') :?>
					
					<a class="noexternal" href="<?=$file[url]?>" target="_blank">
						<div class="deco-header row">
							<?php the_title(); ?>
						</div>
						
						<?php if($thumb) :?>
							<img src="<?=$thumb[sizes][medium]?>" />
						<?php endif; ?>
						
						<?php if(get_the_content()) :?>
							<div class="coupon-content">
								<?php the_content(); ?>
							</div>
						<?php endif; ?>
					</a>
				<?php endif; ?>
			<?php else : ?>
					<div class="deco-header row">
						<?php the_title(); ?>
					</div>
					
					<?php if(get_the_content()) :?>
						<div class="coupon-content">
							<?php the_content(); ?>
						</div>
					<?php endif; ?>
			<?php endif; ?>
			
			<?php // pre_print($file, false);?>
		</div>
	<?php endforeach;
	wp_reset_postdata(); ?>
</div>
