<?php

$args = array(
	'posts_per_page'   => -1,
	'offset'           => $offset,
	'category'         => $category,
	'category_name'    => $category_name,
	'orderby'          => 'menu_order',
	'order'            => 'ASC',
	'post_type'        => 'staff',
	'post_status'      => 'publish',
	'meta_query' => array(
						array(
							'key' => 'location', // name of custom field
							'value' => '"' . $loc_ID . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
							'compare' => 'LIKE'
						)
					)
	);

?>
<ul class="staff-list clearfix">
	<?php
	global $post;
	$myposts = get_posts( $args );
	$count = 0;
	foreach ( $myposts as $post ) :
		setup_postdata( $post );
		$post_ID 	=	get_the_ID();
		$count++;
		if (has_post_thumbnail( $post_ID ) )
		{
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_ID ), 'thumbnail' );
		}
		?>
		<li class="col-xs-24 col-sm-8 staff-member">
			<h6><a href="mailto:<?php the_field('email',$post_ID); ?>"><?php the_title(); ?></a></h6>
			<div class="post-excerpt">

				<?=$image[0];?>
				<?php the_field('email',$post_ID); ?><br />
				<?php the_field('position',$post_ID); ?><br />
				<?php the_field('phone_number',$post_ID); ?>
			</div>
		</li>
		<?php if (($count % 3) == 0){?><div class="clearfix"></div><?php } ?>
	<?php endforeach;
	wp_reset_postdata(); ?>
</ul>
